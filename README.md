# Reversi

## Description

This project is a C++/JS project for my third year of Undergraduate in IT studies. Here is the hosted version : https://ernart.gitlab.io/reversi.

This project is made up of :
- the open standard (WebAssembly) used to create our Web GUI.
- the library (SDL2) used to create our GUI.
- the framework (SDL2_gfx) used to draw circles.
- the toolchain (emscripten) used to compile asm.js and WebAssembly for running C++ code on Web.

## Class Diagram

![classDiagram](img/classDiagram.svg)

## Dependencies

- libcpputest-dev
- libsdl2-dev
- libsdl2-gfx-dev
- cmake
- doxygen
- python3

## Installation for CLI, GUI and Tests.

```sh
cd cpp
mkdir build
cd build
cmake ..
make
```

## Installation for Emscripten

After installing emscripten (https://emscripten.org/docs/getting_started/downloads.html#installation-instructions) :

```sh
cd wasm
# To build an Emscripten projet where you play against yourself :
./buildWasm.sh

# To build an Emscripten projet where you play against a Random AI :
./buildWasmRandomAI.sh
```

## Usage in CLI

```sh
# In cpp/build after make.

# To launch the game where you play against yourself :
./reversi-cli

# To launch the game where you play against a Random AI if you want to play as the Black Player :
./reversi-cli -white=1

# To launch the game where you play against a Random AI if you want to play as the White Player :
./reversi-cli -black=1

# To launch the game where you let two Random AI play with each other :
./reversi-cli -black=1 -white=1
```

For each turn the board is printed and if you have available actions they will be printed. You need to select the one you want typing its number. If you want to quit the game you just enter q. -white and -black are the two arguments that can be specified to launch the game with a RandomAI. 2 will be for a possible Monte Carlo AI.

## Usage in GUI

```sh
# In cpp/build after make.

# To launch the game where you play against yourself :
./reversi-gui

# To launch the game where you play against a Random AI :
./reversi-gui-randomAI
```

For each turn the board is printed in the GUI and you can press with the left click in whatever square you would like to make a move. It will be made if it's a valid one. If you click with the scroll wheel you will reset the game, and pressing the right click or closing the window will exit the game. You have the score and the current player printed in the CLI.

## Usage in Emscripten

```sh
# In project root directory
python3 -m http.server --directory .
# Open in browser http://0.0.0.0:8000/, you can then Play the game (Read the documentation link is broken in local because of the project structure. )
```

The usage in Emscripten is very similar to the GUI one except the right click is disabled. When the party is over you can reset the game with the scroll wheel click. The console output in the bottom of the page will keep track of the score and the current player.

Or you can access it with this direct link : https://ernart.gitlab.io/reversi/wasm/reversi-wasm.html.


## Documentation

```sh
cd cpp
doxygen
cd doc/html
python3 -m http.server --directory .
# Open in browser http://0.0.0.0:8000/
```

Or you can access it with this direct link : https://ernart.gitlab.io/reversi/doc/index.html. When used in direct link lack of classes graphs, they should be there. Need to fix this issue.

# Bilan de projet

## Fonctionnalités disponibles

- CLI utilisable en multijoueur local.
- GUI utilisable en multijoueur local avec gestion des clics et possibilité de redémarrer le jeu en pleine partie.
- GUI utilisable contre une IA Random avec gestion des clics et possibilité de redémarrer le jeu en pleine partie.
- Version Emscripten en multijoueur local pour jouer sur un navigateur.
- Version Emscripten contre une IA Random pour jouer sur un navigateur.
- Documentation claire et détaillée des différentes classes.

## Fonctionnalités à faire et bugs

- Implémentation d'une IA Monte Carlo pour jouer en tant que Joueur Blanc.
- La documentation génère des png en local pour les graphes de classes mais ils ne sont déployés malgré l'utilisation du même script. Après longue lecture l'erreur vient de là :  Generating indexsh: 1: sh: 1: dot: not found. Je dois donc installer graphviz dans mon job doc_build_cpp.
- En ayant utilisé le README.md comme documentation de Doxygen et en donnant une image et un chemin valide la page web de la documentation n'affiche pourtant pas le diagramme de classe.

## Implémentation

Nous utilisons deux classes accompagnée d'une sous-classe et énumération.

### Player : Énumération
#### Valeurs :
- PlayerBlack : Joueur noir.
- PlayerWhite : Joueur blanc.
- PlayerEmpty : Case vide.
- PlayerTie : Égalité.

### VectorPair : Classe
#### Attributs : 
- std::pair<int, int> _availableSquare : Paire qui représente les coordonnées du coup possible.
- std::vector<std::pair<int, int>> _flippedSquares : Vecteur de paire qui stocke tous les points retournés par le coup possible _availableSquare.

### Reversi : Classe
#### Attributs :
- std::array<std::array<Player, 8>, 8> _board : Double tableau de notre énumération Player, notre tableau de jeu.
- Player _winner : Variable qui stocke le joueur qui gagne la partie ainsi que l'égalité entre les joueurs ou Empty si la partie n'est pas terminée.
- Player _current : Variable qui stocke le joueur courant.
- std::vector<VectorPair> _moves : Vecteur de VectorPair qui nous permet de stocker tous les coups possibles pour un Joueur avec pour chaque coup ses cases retournées.
- int _scoreBlack : Score du joueur noir.
- int _scoreWhite : Score du joueur blanc.
- int _isImpossiblePlay : Flag qui nous permet de vérifier si on saute un tour. Quand on en saute deux c'est que la partie ne peut pas continuer donc on arrête la partie.
- bool _isFinished : Booléen qui nous permet de savoir si la partie est terminée.

#### Méthodes :
- Reversi() : Constructeur qui appelle la fonction resetGame.
- virtual void resetGame() : Fonction qui remet le _board en état initial et remet les scores et les flags à 0. Fonction virtuelle pour pouvoir l'override dans sa classe fille.
- Player getWinner() : Fonction qui appelle updateScore et qui va vérifier si l'addition des scores atteint 64 ou si les joueurs peuvent encore jouer, et renvoie le joueur gagnant.
- Player getCurrentPlayer() const : Fonction qui renvoie le joueur courant.
- void updateScore() : Fonction qui va dans tout le tableau compter chaque éléments et ainsi actualiser le score.
- int getScoreBlack() const : Fonction qui renvoie le score du joueur noir.
- int getScoreWhite() const : Fonction qui renvoie le score du joueur blanc.
- std::vector<VectorPair> getMoves() const : Fonction qui renvoie le std::vector<VectorPair> _moves.
- bool makeMove(int i, int j) : Fonction qui fait une action en vérifiant que les coordonnées données sont bien dans _moves. Si le tour a été effectué on change de joueur.
- bool isGameOver() : Fonction qui lance getWinner pour renvoyer si oui ou non la partie est terminée.
- std::vector<std::pair<int, int>> findPossibleSquares() : Fonction qui va chercher toutes les cases vides adjacentes à des cases opposées.
- bool findUsableSquares(std::vector<std::pair<int, int>> possibleSquares) : Fonction qui prend en paramètre le std::vector<std::pair<int, int>> de findPossibleSquares, va pour chaque point aller dans _board et partir dans les 8 directions pour s'assurer que le coup est valide. Si c'est le cas on stocke dans un VectorPair le coup valide dans _availableSquare et ses pièces retournées dans _flippedSquares, qu'on ajoute à notre _moves. S'il n'y a pas de coups jouables on change de joueur.

### ReversiGUI : Classe
#### Attributs :
- std::array <std::array <SDL_Rect*, 8>, 8> _boardGUI : Double tableau de pointeurs SDL_Rect, notre tableau d'affichage.
- SDL_Window* _pWindow : Pointeur de SDL_Window qui renvoie le pointeur de notre fenêtre actuelle.
- SDL_Renderer* _pRenderer : Pointeur de SDL_Renderer qui renvoie le pointeur de notre renderer actuel.

#### Méthodes :
- ReversiGUI() : Constructeur qui initialise la fenêtre avec notre _pRenderer et _pWindow, qui dessine la fenêtre de background en vert et qui lance deux fonctions, constructBoardGUI et printBoard.
- ~ReversiGUI() : Destructeur qui va supprimer pour chaque case de _boardGUI le SDL_Rect associé, puis détruire le Renderer et le Window et quitter l'application.
- void resetGame() override : Fonction qui appelle le resetGame de Reversi et qui va remettre en vert le background de la fenêtre avant d'appeler constructBoardGUI.
- SDL_Window* getWindow() const : Fonction qui renvoie le pointeur de notre fenêtre actuelle.
- SDL_Renderer* getRenderer() const : Fonction qui renvoie le pointeur de notre renderer actuel.
- void constructBoardGUI() : Fonction qui dessine sur notre fenêtre une grille de rectangles de contours noirs.
- std::pair<int, int> mouseInUsableSquare(SDL_Point mousePosition) : Fonction qui vérifie la position de la souris avec le point donné en paramètre et qui va chercher dans notre _boardGUI si ce point est dedans. Si c'est le cas on vérifie que la case cliquée est dans _moves, si oui on renvoie la paire qui correspond au coup. Sinon on en renvoie une incorrecte.
- void printBoard() : Fonction qui affiche la grille en dessinant avec SDL_GFX des cercles de couleurs noirs et blancs en fonction de _board. On utilise un ifdef pour vérifier si on utilise Emscripten, si oui on redessine notre fenêtre en vert, on reconstruit la grille et on affiche le score des joueurs pour ne l'afficher qu'une fois dans Emscripten.

### RandomAI : Classe
#### Méthodes :
- RandomAI() : Constructeur qui est vide car cette classe n'a pas d'attributs.
- int pickRandomNumber(std::vector<VectorPair> & moves); : Fonction qui renvoie un numéro aléatoire entre 0 et moves.size() non inclut.

Nous avons trois main qui lancent deux codes différents.

### reversi-cli
#### Fonctionnement :
- int main() : Lance la fonction printGame tant que le jeu n'est pas terminé ou qu'il n'a pas été fermé puis affiche le score en fonction du vainqueur.
- void printGame(Reversi & party, bool & isClosed, , int playerBlack, int playerWhite) : Fonction qui va lancer le tour pour le joueur courant en lui proposant des actions possibles, l'utilisateur peut entrer q pour quitter la partie. S'il n'y a aucune action jouable le tour est sauté. On vérifie si playerBlack ou playerWhite sont différents de 0 on vérifie le joueur courant associé à ces variables et on demande à notre objet random de choisir un nombre avec pickRandomNumber. On spécifie au programme quel joueur est géré par une IA Random avec un argument en lançant la fonction. Si aucun argument n'est mit on joue en multijoueur local.

### reversi-gui
#### Fonctionnement :
- int main() : On utilise un ifdef pour spécifier deux cas dans notre main. Si on utilise Emscripten on lance une boucle emscripten_set_main_loop qui lance la fonction printGameEm car une boucle infinie ne peut pas marcher dans Emscripten (on déclare des variables globales pour les utiliser dans cette fonction). Si on ne l'utilise pas on utilise un fonctionnement similiaire que dans notre GUI où on lance la fonction printGame tant que la partie n'est pas terminée, en vérifiant que si on lance un signal de fermeture de fenêtre on casse la boucle. On affiche ensuite le score (une seule fois grâce au booléen isFinished) puis on attend un input du joueur pour fermer ou redémarrer le jeu.
- void printGame(Reversi & party, bool & isClosed) : Fonction qui va lancer le tour pour le joueur courant. L'utilisateur peut faire un clic gauche pour jouer un coup, un clic du milieu pour redémarrer la partie et un clic droit pour quitter la partie. S'il n'y a aucune action jouable le tour est sauté.
- void printGameEm() : Fonction qui va lancer le tour pour le joueur courant. On allège la fonction en retirant les affichages car cette fonction sera lancée toutes les secondes pour éviter que les affichages soient renouvellés chaque frame. On vérifie les coups et on laisse la boucle d'input pour atteindre un clic de l'utilisateur. Si le coup est valide on affiche le tableau (la fonction printBoard est modifiée pour afficher le score et le tableau grâce à ifdef).

### reversi-gui-randomAI
#### Fonctionnement :
Même principe que reversi-gui, il y a juste ces quelques modifications :
- void printGame(Reversi & party, bool & isClosed, RandomAI & randomAI) : Fonction qui va lancer le tour pour le joueur courant. Si le joueur est blanc alors il utilise la méthode pickRandomNumber de l'objet randomAI pour choisir un coup. L'utilisateur peut faire un clic gauche pour jouer un coup, un clic du milieu pour redémarrer la partie et un clic droit pour quitter la partie. S'il n'y a aucune action jouable le tour est sauté.
- void printGameEm() : Fonction qui va lancer le tour pour le joueur courant. On allège la fonction en retirant les affichages car cette fonction sera lancée toutes les secondes pour éviter que les affichages soient renouvellés chaque frame. On vérifie les coups et on laisse la boucle d'input pour atteindre un clic de l'utilisateur. Si le joueur est blanc alors il utilise la méthode pickRandomNumber de l'objet randomAI pour choisir un coup, afficher le tableau et arrêter la fonction pour passer au tour suivant. Si le coup est valide on affiche le tableau (la fonction printBoard est modifiée pour afficher le score et le tableau grâce à ifdef).

## Retour d'expérience

### Déroulement

Ayant regardé le fonctionnement de SDL pour prévoir les attributs graphiques j'ai du malgré tout modifié ma classe Reversi quelques fois pour changer mes paramètres en protected et pouvoir y accéder dans ma classe ReversiGUI. Pour l'Emscripten j'ai pu utiliser des ifdef pour garder le fonctionnement de ma GUI sans devoir créer de nouveaux codes ou nouvelles classes et que reversi-gui soit adaptée à la SDL et Emscripten.

### Problèmes rencontrés

- La CLI s'est bien passée, le plus complexe était de visualiser une classe efficace et de pouvoir aisément vérifier les coups possibles pour chaque action. C'est de là d'où m'est venue l'idée d'avoir deux fonctions séparées qui vont fonctionner en tandem et ainsi s'assurer que le coup que l'on fasse soit valide. Le problème est venu quand après d'autres essais du jeu j'ai remarqué qu'une seule direction était prise en compte alors que mon algorithme allait bien dans chaque direction. Il s'est avéré que je copiais mon vecteur temporaire au vecteur principal au lieu de le rajouter, cette simple correction a permit de corriger le moteur de jeu. Ce fut également le cas quand la partie se terminait car les deux joueurs ne peuvent plus jouer, il a fallu rajouter cette condition. J'ai également retiré l'utilisation du exit(0) pour quitter le programme car cela laissait l'OS décider comment vider la mémoire au lieu d'utiliser un return 0 bien plus propre.

- La GUI fut plus complexe car il fallait comprendre SDL2. Entre affichage incorrect car SDL2 gère différemment l'affichage qu'une simple CLI, le changement de notre image d'alpine à gcc pour installer correctement SDL2, le plus embêtant fut la gestion des évènements. En l'état elle est assez spartiate, demandant une vérification dans la fonction lancée en boucle et la seconde à la fin du jeu pour proposer au joueur de relancer une partie ou de la quitter.

- Le service Web Emscripten fut sûrement le plus dur à implémenter. Réadapter mon code déjà existant fut assez long et je tentais d'utiliser un Cmake pour pouvoir le compiler, ce qui m'a fait perdre bien plus de temps que prévu. Finalement un simple script shell est plus pratique dans mon cas, et le code utile pour Emscripten peut être encapsulé dans des ifdef pour ainsi avoir deux codes distincts dans le même fichier. Emscripten demandait de changer la façon d'afficher pour ne le faire qu'une fois le clic activé et éviter de reconstruire le tableau et chaque cercle toutes les frames, ici on ne relance en permanence que les events et on peut ainsi économiser de la mémoire.

## Conclusion

Ce projet fut très intéressant et utile pour mieux appréhender un futur projet. En effet, j'ai pu ainsi travaillé sur SDL2 et Emscripten et bien mieux comprendre leurs fonctionnements. Je continue de travailler sur les IA car j'aimerais énormément en implémenter une dans mon jeu, le problème étant que je n'arrive pas à visualiser comment les intégrer dans mon jeu actuel et qu'ils jouent à la place d'un joueur humain. Peut-être un simple if quand le joueur est blanc pour changer le fonctionnement du choix dans printGame ? Notre classe Reversi garde tous les coups avec _moves donc on peut aisément récupérer _moves dans une classe IA et ainsi traiter les données.

Après avoir implémenté une IA Random j'ai juste eu besoin de faire un if en fonction du joueur et de lancer une fonction très simple. L'IA de Monte Carlo est bien plus complexe et va me demander bien plus de travail, mais j'ai au moins une très simple IA pour donner une meilleure intéractivité à mon projet, tout en laissant la possibilité de jouer sans dans chacun des trois modes de fonctionnements, CLI, GUI et Emscripten.