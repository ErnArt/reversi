#ifndef REVERSIGUI_HPP_
#define REVERSIGUI_HPP_

#include <SDL2/SDL.h>
#include <SDL2/SDL2_gfxPrimitives.h>
#include "Reversi.hpp"

/**
 * @brief Primary class for the GUI.
 * 
 * This class implements the GUI to let the player play. We inherit it from Reversi to add the GUI without rewriting the whole game engine. 
 */
class ReversiGUI : public Reversi
{
	private:
		std::array <std::array <SDL_Rect*, 8>, 8> _boardGUI;
		SDL_Window* _pWindow;
		SDL_Renderer* _pRenderer;

	public:
		/**
		 * @brief Construct a new Reversi GUI object, initialising the game.
		 */
		ReversiGUI();

		/**
		 * @brief Destroy the Reversi GUI object to avoid memory leaks.
		 */
		~ReversiGUI();

		/**
		 * @brief Reset the game. Overriding of the method in Reversi.
		 */
		void resetGame() override;

		/**
		 * @brief Get the SDL_Window pointer _pWindow.
		 * 
		 * @return SDL_Window* _pWindow.
		 */
		SDL_Window* getWindow() const;

		/**
		 * @brief Get the SDL_Renderer pointer _pRenderer.
		 * 
		 * @return SDL_Renderer* _pRenderer.
		 */
		SDL_Renderer* getRenderer() const;

		/**
		 * @brief Create the whole board with black non filled rectangles to make a grid.
		 */
		void constructBoardGUI();

		/**
		 * @brief Verifies the mouse position with a valid square and returns the coordinates of the usable square.
		 * 
		 * @param mousePosition an SDL_Point that has the mouse coordinates.
		 * @return std::pair<int, int> the coordinates of the usable square to make a move or return a wrong pair to catch it in our main.
		 */
		std::pair<int, int> mouseInUsableSquare(SDL_Point mousePosition);

		/**
		 * @brief Print the board with evaluating the _board from the game engine.
		 */
		void printBoard();

};

#endif