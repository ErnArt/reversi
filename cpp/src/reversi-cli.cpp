#include <iostream>
#include "Reversi.hpp"
#include "RandomAI.hpp"


using namespace std;

void printGame(Reversi & party, bool & isClosed, int playerBlack, int playerWhite)
{
	cout<<party<<endl;
	cout<<"Score of Black : "<< party.getScoreBlack()<<endl;
	cout<<"Score of White : "<< party.getScoreWhite()<<endl<<endl;
	cout<<"Current Player : ";
	if (party.getCurrentPlayer() == PlayerBlack)
		cout<<"Black"<<endl;
	else
		cout<<"White"<<endl;
	cout<<endl;

	std::vector<std::pair<int, int>> possibleSquares = party.findPossibleSquares();
	party.findUsableSquares(possibleSquares);
	std::vector<VectorPair> usableSquares = party.getMoves();

	// If the current player can make an action
	if (usableSquares.size() != 0)
	{
		// If one of the player is controlled by an AI.
		if ((playerWhite != 0 && party.getCurrentPlayer() == PlayerWhite) || (playerBlack != 0 && party.getCurrentPlayer() == PlayerBlack))
		{
			RandomAI randomAI;
			int index = randomAI.pickRandomNumber(usableSquares);
			party.makeMove(usableSquares.at(index)._availableSquare.first, usableSquares.at(index)._availableSquare.second);
		}
		else
		{
			cout<<"Actions : "<<endl;
			for (unsigned i=0; i<usableSquares.size(); i++)
			{
				cout<<"Move "<<i<<" : "<<endl;
				cout<<usableSquares.at(i)._availableSquare.first<<"."<<usableSquares.at(i)._availableSquare.second<<" returns -> ";
				for (auto e : usableSquares.at(i)._flippedSquares)
				{
					cout<<e.first<<"."<<e.second<<" ";
				}
				cout<<endl<<endl;
			}
			
			// Use of stringstream for converting our input as any int, using a char would not be efficient (because we could input 10 and it would not be counted)
			string cinResult;
			cout<<"Which move do you want to do ?"<<endl;
			cin>>cinResult;

			while(cinResult != "q" && (stoi(cinResult) > (int)usableSquares.size()-1 || stoi(cinResult) < 0))
			{
				cout<<endl<<"Incorrect move. Please input another move."<<endl;
				cin>>cinResult;
			}

			// To quit the main program, exit(0) to avoid because it let the OS call the destructor, not reliable.
			if (cinResult == "q")
			{
				isClosed = true;
				return;
			}

			int convertedCinRes = stoi(cinResult);
			party.makeMove(usableSquares.at(convertedCinRes)._availableSquare.first, usableSquares.at(convertedCinRes)._availableSquare.second);
			cout<<endl;
		}
	}
}

int main(int argc, char* argv[])
{
	// We can use optionnal arguments to let the game use AI or not. First we create an int for each player.
	int playerBlack = 0, playerWhite = 0;

	// If we only have ./reversi-cli we don't enter in this loop.
	for (int i=1; i<argc; i++)
	{
		// We then convert our argv[i] to an string to use substr.
		string initial = argv[i];
		string player = initial.substr(1, initial.find("=")-1);

		// We extract the current argument of the command, to specify which player's int is modified.
		if (player == "black")
		{
			playerBlack = stoi(initial.substr(initial.find("=")+1, 1));
		}
		if (player == "white")
		{
			playerWhite = stoi(initial.substr(initial.find("=")+1, 1));
		}
	}

	Reversi party;
	bool isClosed = false;

	while (!party.isGameOver() && isClosed == false)
		printGame(party, isClosed, playerBlack, playerWhite);

	cout<<endl<<endl<<party<<endl;
		
	cout<<endl<<endl<<"Final board"<<endl<<party<<endl<<"And the winner is..."<<endl;
	Player winner = party.getWinner(); 
	switch(winner)
	{
		case PlayerBlack:
			cout<<"Black !"<<endl;
			break;
		case PlayerWhite:
			cout<<"White !"<<endl;
			break;
		case PlayerTie:
			cout<<"No one !"<<endl;
			break;
		// This case shouldn't exist.
		default:
			cout<<"No one, because the game was closed."<<endl;
	}

	cout<<"With "<< party.getScoreBlack()<<" for the Black."<<endl;
	cout<<"With "<< party.getScoreWhite()<<" for the White."<<endl;
	cout<<endl;

	return 0;
}