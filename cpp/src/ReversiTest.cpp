#include <sstream>
#include "Reversi.hpp"

#include <CppUTest/CommandLineTestRunner.h>

TEST_GROUP(GroupReversi) { };

/**
 * @brief Simple function that will verify with a given string a default Reversi game.
 * 
 * We create an stringstream object to go through all the game and then compare it to the given string.
 * 
 * @param str the string we want to compare.
 * @param game a default Reversi game.
 */
void testGame(const std::string & str, const Reversi & game)
{
	std::stringstream oss;
	oss << game;
	CHECK_EQUAL(str, oss.str());
}

TEST(GroupReversi, printTest)
{
	Reversi game;
	
	// Verification of the printing when launching a new game.
	testGame(". . . . . . . . \n. . . . . . . . \n. . . . . . . . \n. . . W B . . . \n. . . B W . . . \n. . . . . . . . \n. . . . . . . . \n. . . . . . . . \n", game);
}

TEST(GroupReversi, scoreTestBlack)
{
	Reversi game;
	CHECK_EQUAL(2, game.getScoreBlack());
}

TEST(GroupReversi, scoreTestWhite)
{
	Reversi game;
	CHECK_EQUAL(2, game.getScoreWhite());
}

TEST(GroupReversi, beginCurrentPlayerTest)
{
	Reversi game;
	CHECK_EQUAL(PlayerBlack, game.getCurrentPlayer());
}

TEST(GroupReversi, beginWinnerPlayerTest)
{
	Reversi game;
	CHECK_EQUAL(PlayerEmpty, game.getWinner());
}

TEST(GroupReversi, beginIsOverTest)
{
	Reversi game;
	CHECK_EQUAL(false, game.isGameOver());
}