#ifndef RANDOM_AI_HPP_
#define RANDOM_AI_HPP_

#include <iostream>
#include <cstdlib>
#include <ctime>
#include "Reversi.hpp"

/**
 * @brief Class for a randomAI. 
 * 
 * If I implement the Monte Carlo one I could make a mother class AI and RandomAI and MonteCarloAI 
 * inheriting from AI.
 */
class RandomAI
{
	public:
		/**
		 * @brief Construct a new Random AI object. Empty.
		 */
		RandomAI() {};

		/**
		 * @brief Pick and return a random number for an action.
		 * 
		 * @param moves the vector of all possibles moves to make a move.
		 * @return int an index between 0 and moves.size that will be used as a move.
		 */
		int pickRandomNumber(std::vector<VectorPair> & moves);
};

#endif