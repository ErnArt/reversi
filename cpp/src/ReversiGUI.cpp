#include "ReversiGUI.hpp"

ReversiGUI::ReversiGUI()
{
	// If we can't initialise our Window
	if (SDL_Init(SDL_INIT_VIDEO) < 0)
	{
		SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "[DEBUG] > %s", SDL_GetError());
		SDL_Quit();
	}

	if (SDL_CreateWindowAndRenderer(600, 600, SDL_WINDOW_SHOWN, &_pWindow, &_pRenderer) < 0)
	{
		SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "[DEBUG] > %s", SDL_GetError());
		SDL_Quit();
	}

	SDL_SetWindowTitle(_pWindow, "Reversi");

	// We put our background color to green.
	SDL_SetRenderDrawColor(_pRenderer, 0, 255, 0, 255);
	
	// Refresh the main window with the previously given color.
	SDL_RenderClear(_pRenderer);

	constructBoardGUI();

	printBoard();
}

ReversiGUI::~ReversiGUI()
{
	for (auto l : _boardGUI)
		for (auto c : l )
			delete c;
	SDL_DestroyRenderer(_pRenderer);
	SDL_DestroyWindow(_pWindow);
	SDL_Quit();
}

void ReversiGUI::resetGame()
{
	// We call the resetGame function from the mother class Reversi.
	Reversi::resetGame();

	// We then redraw our window and reconstruct the board.
	SDL_SetRenderDrawColor(_pRenderer, 0, 255, 0, 255);
	SDL_RenderClear(_pRenderer);
	constructBoardGUI();
}

SDL_Window* ReversiGUI::getWindow() const { return _pWindow; }

SDL_Renderer* ReversiGUI::getRenderer() const { return _pRenderer; }

void ReversiGUI::constructBoardGUI()
{
	for (int i=0; i<8; i++)
	{
		for (int j=0; j<8; j++)
		{
			// We divide by 8 our window height to get the best size for our rectangles.
			_boardGUI[i][j] = new SDL_Rect{i*600/8, j*600/8, 600/8, 600/8};
		}
	}

	for (auto l : _boardGUI)
	{
		for (auto c : l )
		{
			// Draw the non filled rectangles with black.
			SDL_SetRenderDrawColor(_pRenderer, 0, 0, 0, 255);
			SDL_RenderDrawRect(_pRenderer, c);
			SDL_RenderPresent(_pRenderer);
		}
	}
}

void ReversiGUI::printBoard()
{
	// Use of ifdef to add these portion of code when using emscripten. We need to redraw the board to avoid wrong background colors.
	#ifdef __EMSCRIPTEN__
		SDL_SetRenderDrawColor(_pRenderer, 0, 255, 0, 255);
		SDL_RenderClear(_pRenderer);
		constructBoardGUI();
	#endif

	// We need to print the GUI board in reverse bcause of the difference between SDL and CLI printing.
	for (int i=0; i<8; i++)
	{
		for (int j=0; j<8; j++)
		{
			if (_board[i][j] == PlayerBlack)
				// Here were adding half a point to our coordinate to point to have it centered.
				filledCircleRGBA(_pRenderer, (j*600/8)+((600/8)/2), (i*600/8)+((600/8)/2), 30, 0, 0, 0, 255);
				
			if (_board[i][j] == PlayerWhite)
				filledCircleRGBA(_pRenderer, (j*600/8)+((600/8)/2), (i*600/8)+((600/8)/2), 30, 255, 255, 255, 255);
		}
	}

	// Use of ifdef to add these portion of code when using emscripten. We then are able to print the score of each player in each turn.
	#ifdef __EMSCRIPTEN__
		updateScore();
		std::cout<<"Score of Black : "<< getScoreBlack()<<std::endl;
		std::cout<<"Score of White : "<< getScoreWhite()<<std::endl<<std::endl;
		std::cout<<"Current Player : ";
		if (getCurrentPlayer() == PlayerBlack)
			std::cout<<"Black"<<std::endl;
		else
			std::cout<<"White"<<std::endl;
		std::cout<<std::endl;
	#endif

	// Refresh the GUI.
	SDL_RenderPresent(_pRenderer);
}

std::pair<int, int> ReversiGUI::mouseInUsableSquare(SDL_Point mousePosition)
{
	// We need to go through our whole _boardGUI, if the mouse is in a SDL_Rect we then verify if it's coordinates are a valid point in _availableSquare
	for (int i=0; i<8; i++)
	{
		for (int j=0; j<8; j++)
		{
			// When we find that the mouse in is one rectangle.
			if (SDL_PointInRect(&mousePosition, _boardGUI[i][j]))
			{
				// We verify if the coordinates are representing a usable square in our _moves vector.
				// We need to invert the coordinates because of how the grid is inverted when printed.
				std::pair<int, int> mousePair = std::make_pair(j, i);
				
				for (auto e : _moves)
				{
					// If we find that the mouse clicked a valid square we return it.
					if (e._availableSquare == mousePair)
						return e._availableSquare;
				}
				
			}
		}
	}

	// If we can't find the square that the mouse clicked we return a wrong pair.
	return std::make_pair(-1, -1);
}