#ifndef VECTORPAIR_HPP_
#define VECTORPAIR_HPP_

#include <iostream>
#include <utility>
#include <vector>

/**
 * @brief Secondary class for storing possible actions.
 * 
 * In this class we can have for one available square (or one action) multiple returned squares
 * this is very useful to quickly scan the whole array and then flip the squares.
 */
class VectorPair
{
	public:
		std::pair<int, int> _availableSquare;
		std::vector<std::pair<int, int>> _flippedSquares;
};

#endif

#ifndef REVERSI_HPP_
#define REVERSI_HPP_

#include <iostream>
#include <array>
#include <utility>
#include <vector>

enum Player {PlayerBlack, PlayerWhite, PlayerEmpty, PlayerTie};

using Board = std::array <std::array <Player, 8>, 8>;

/**
 * @brief Primary class for the Game Engine and the CLI.
 * 
 * This class implements the CLI and the Game Engine to allow the player to playing. With our _board being the are of playing,
 * _winner and _current our Player value, _moves to store every possible action to list them to the player, _scoreBlack and _scoresWhite
 * to store their points, _isImpossiblePlay verifying if the player can still play or if the game is over (even without having
 * 64 as a addition of their score) and _isFinished to tell our program about the end of the party.
 */
class Reversi
{
	protected:
		Board _board;
		Player _winner, _current;
		std::vector<VectorPair> _moves;
		int _scoreBlack, _scoreWhite, _isImpossiblePlay;
		bool _isFinished;

	public:
		/**
		 * @brief Construct a new Reversi object, initialising the game.
		 */
		Reversi();

		/**
		 * @brief Reset the game. Virtual function to override it in our ReversiGUI class.
		 */
		virtual void resetGame();

		/**
		 * @brief Get the Winner object.
		 * 
		 * @return the winner (Black, White, Tie) or Empty if the party is not over.
		 */
		Player getWinner();

		/**
		 * @brief Get the Current Player object.
		 * 
		 * @return the current player (Black, White).
		 */
		Player getCurrentPlayer() const;

		/**
		 * @brief Update the score for all players.
		 */
		void updateScore();
		
		/**
		 * @brief Get the Score of the playerBlack.
		 * 
		 * @return score of the playerBlack.
		 */
		int getScoreBlack() const;


		/**
		 * @brief Get the Score of the playerWhite.
		 * 
		 * @return score of the playerWhite.
		 */
		int getScoreWhite() const;
		
		/**
		 * @brief Get the Moves object.
		 * 
		 * @return std::vector<VectorPair> the possibles moves available. 
		 */
		std::vector<VectorPair> getMoves() const;
		
		/**
		 * @brief Make a move for the current player.
		 * 
		 * @param x line selected (0 to 7).
		 * @param y column selected (0 to 7).
		 * @return true if the move is valid.
		 * @return false if the move is invalid.
		 */
		bool makeMove(int x, int y);

		/**
		 * @brief Verifies is the party is over.
		 * 
		 * @return true if the game is over.
		 * @return false if the game is not over.
		 */
		bool isGameOver();

		/**
		 * @brief Find possible squares to store into a vector.
		 * 
		 * This function searches all the opposite squares and will search if they have empty squares next to them.
		 * It then adds them to a vector to be possible in a verifying function.
		 * 
		 * @return std::vector<std::pair<int, int>> of the possibles squares.
		 */
		std::vector<std::pair<int, int>> findPossibleSquares();

		/**
		 * @brief Find usable squares from the possible squares and store them into _moves.
		 * 
		 * This function verifies each possible squares and then only keeps the real usable squares.
		 * 
		 * @return true if there are usable squares.
		 * @return false if there is no more usable squares.
		 */
		bool findUsableSquares(std::vector<std::pair<int, int>> possibleSquares);

		/**
		 * @brief Redeclaration of operator<< as friend to access the _board attribute.
		 * 
		 * @param os ostream operator.
		 * @param game the object we want to print.
		 * @return std::ostream& the ostream operator used to chain various << operation.
		 */
		friend std::ostream & operator<<(std::ostream & os, const Reversi & game);
};

/**
 * @brief Redeclaration of operator<< to print the _board.
 * 
 * @param os ostream operator.
 * @param game the object we want to print.
 * @return std::ostream& the ostream operator used to chain various << operation.
 */
std::ostream & operator<<(std::ostream & os, const Reversi & game);

#endif