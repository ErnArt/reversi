#include <iostream>
#include "ReversiGUI.hpp"

// Use of multiple ifdef to specialize the code when using em++ instead of c++.
#ifdef __EMSCRIPTEN__
	#include <emscripten.h>
	// Declaration of global variables to use them in printGameEm because we can't give arguments to a function using emscripten.
	ReversiGUI party;
	SDL_Event events;
	bool isFinished = false;
#endif

using namespace std;

// We pass isOpen as a reference to be able to modify it. We pass randomAI to not have to copy it multiple time.
void printGame(ReversiGUI & party, bool & isOpen)
{
	party.printBoard();
	cout<<"Score of Black : "<< party.getScoreBlack()<<endl;
	cout<<"Score of White : "<< party.getScoreWhite()<<endl<<endl;
	cout<<"Current Player : ";
	if (party.getCurrentPlayer() == PlayerBlack)
		cout<<"Black"<<endl;
	else
		cout<<"White"<<endl;
	cout<<endl;

	std::vector<std::pair<int, int>> possibleSquares = party.findPossibleSquares();
	party.findUsableSquares(possibleSquares);
	std::vector<VectorPair> usableSquares = party.getMoves();

	// If the current player can make an action
	if (usableSquares.size() != 0)
	{
		SDL_Event events; 
		SDL_bool run = SDL_TRUE;

		while (run)
		{
			while (SDL_PollEvent(&events))
			{
				switch(events.type)
				{
					// We stop the function if we receive the close window signal.
					case SDL_QUIT :
						isOpen = false;
						return;

					// We divide our case in three distincts actions.
					case SDL_MOUSEBUTTONDOWN :
						// When there is a left click we create a point to see if it's in the board.
						if (events.button.button == SDL_BUTTON_LEFT)
						{
							SDL_Point mousePosition = {events.button.x, events.button.y};
							pair<int, int> mousePair = party.mouseInUsableSquare(mousePosition);
							if (mousePair.first != -1 && mousePair.second != -1)
							{
								party.makeMove(mousePair.first, mousePair.second);
								return;
							}
							break;
						}

						// When there is a middle click we reset the game.
						if (events.button.button == SDL_BUTTON_MIDDLE)
						{
							party.resetGame();
							return;
						}

						// Where there is a right click we close the main window.
						if (events.button.button == SDL_BUTTON_RIGHT)
						{
							isOpen = false;
							return;
						}
				}
			}
		}
		cout<<endl;
	}
}

// We use a different function if we are using emscripten, a stripped down version of printGame with a if statement to update the score,
// using the function isGameOver and then being able to print the results.
#ifdef __EMSCRIPTEN__
	void printGameEm(void)
	{
		if (!party.isGameOver())
		{
			std::vector<std::pair<int, int>> possibleSquares = party.findPossibleSquares();
			party.findUsableSquares(possibleSquares);
			std::vector<VectorPair> usableSquares = party.getMoves();

			// If the current player can make an action
			if (usableSquares.size() != 0)
			{
				while (SDL_PollEvent(&events))
				{
					switch(events.type)
					{
						// We divide our case in three distincts actions.
						case SDL_MOUSEBUTTONDOWN :
							// When there is a left click we create a point to see if it's in the board.
							if (events.button.button == SDL_BUTTON_LEFT)
							{
								SDL_Point mousePosition = {events.button.x, events.button.y};
								pair<int, int> mousePair = party.mouseInUsableSquare(mousePosition);
								if (mousePair.first != -1 && mousePair.second != -1)
								{
									party.makeMove(mousePair.first, mousePair.second);
									party.printBoard();
									return;
								}
								break;
							}

							// When there is a middle click we reset the game.
							if (events.button.button == SDL_BUTTON_MIDDLE)
							{
								party.resetGame();
								party.printBoard();
								return;
							}
					}
				}
			}
		}
		else
		{
			// Because we will stay in the while loop waiting an action of the player, we can print the score only once like that.
			if (!isFinished)
			{
				party.printBoard();
				cout<<endl<<endl<<"And the winner is..."<<endl;
				Player winner = party.getWinner(); 
				switch(winner)
				{
					case PlayerBlack:
						cout<<"Black !"<<endl;
						break;
					case PlayerWhite:
						cout<<"White !"<<endl;
						break;
					case PlayerTie:
						cout<<"No one !"<<endl;
						break;
					// This case shouldn't exist.
					default:
						cout<<"No one, because the game was closed."<<endl;
				}
				cout<<"With "<< party.getScoreBlack()<<" for the Black."<<endl;
				cout<<"With "<< party.getScoreWhite()<<" for the White."<<endl;
				cout<<endl;
				isFinished=true;
			}
			while (SDL_PollEvent(&events))
			{
				switch(events.type)
				{
					case SDL_MOUSEBUTTONDOWN :
						if (events.button.button == SDL_BUTTON_MIDDLE)
						{
							party.resetGame();
							party.printBoard();
							return;
						}
				}
			}
		}
	}
#endif

int main()
{
	#ifdef __EMSCRIPTEN__
		emscripten_set_main_loop(printGameEm, 60, 1);
	#else
		ReversiGUI party;
		SDL_Event events;
		bool isOpen{true};
		bool isFinished = false;
		while (isOpen)
		{
			while (!party.isGameOver())
			{
				printGame(party, isOpen);

				// If the player wants to close the game we just break the while loop, pass the score print and exit the main window.
				// Better way than use exit(0) from the function because here we call the destructor to clean the memory.
				if (isOpen == false)
					break;
			}
			
			// Because we will stay in the while loop waiting an action of the player, we can print the score only once like that.
			if (!isFinished)
			{
				party.printBoard();
				cout<<endl<<endl<<"And the winner is..."<<endl;
				Player winner = party.getWinner(); 
				switch(winner)
				{
					case PlayerBlack:
						cout<<"Black !"<<endl;
						break;
					case PlayerWhite:
						cout<<"White !"<<endl;
						break;
					case PlayerTie:
						cout<<"No one !"<<endl;
						break;
					// This case shouldn't exist.
					default:
						cout<<"No one, because the game was closed."<<endl;
				}
				cout<<"With "<< party.getScoreBlack()<<" for the Black."<<endl;
				cout<<"With "<< party.getScoreWhite()<<" for the White."<<endl;
				cout<<endl;
				isFinished = true;
			}

			// We add this second SDL_PollEvent to watch for input as the end of the party, like closing the window or resetting the game.
			while (SDL_PollEvent(&events))
			{
				switch(events.type)
				{
					// We stop the function if we receive the close window signal.
					case SDL_QUIT :
						isOpen = false;
						break;

					// We divide our case in two distincts actions.
					case SDL_MOUSEBUTTONDOWN :
						// When there is a middle click we reset the game.
						if (events.button.button == SDL_BUTTON_MIDDLE)
						{
							party.resetGame();
							break;
						}

						// Where there is a right click we close the main window.
						if (events.button.button == SDL_BUTTON_RIGHT)
						{
							isOpen = false;
							break;
						}
				}
			}
		}
	#endif
	return 0;
}