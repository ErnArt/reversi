#include "Reversi.hpp"
#include <algorithm>

Reversi::Reversi()
{
	resetGame();
}

void Reversi::resetGame()
{
	for (auto & l : _board)
		for (auto & c : l)
			c = PlayerEmpty;
	_board[3][3] = PlayerWhite;
	_board[3][4] = PlayerBlack;
	_board[4][3] = PlayerBlack;
	_board[4][4] = PlayerWhite;

	//Initialisation of the players values and scores.
	_current = PlayerBlack;
	_winner = PlayerEmpty;
	_scoreBlack = 2;
	_scoreWhite = 2;
	_isImpossiblePlay=0;
	_isFinished = false;
}

Player Reversi::getWinner()
{
	// We update the score when we want to see who is the winner.
	updateScore();

	// If the party is over (by adding their scores or if they can't play anymore).
	if (_scoreBlack + _scoreWhite == 64 || _isImpossiblePlay == 2)
	{
		// We verifies which is the winning player.
		if (_scoreBlack > _scoreWhite)
			_winner = PlayerBlack;
		else if (_scoreBlack < _scoreWhite)
			_winner = PlayerWhite;
		// If no one won then there is a tie.
		else
			_winner = PlayerTie;

		_isFinished = true;
	}
	return _winner;
}

Player Reversi::getCurrentPlayer() const { return _current; }

void Reversi::updateScore()
{
	int countB=0, countW=0;
	for (auto l : _board)
		for (auto c : l)
			if (c == PlayerBlack)
				countB++;
			else if (c == PlayerWhite)
				countW++;
	_scoreBlack = countB;
	_scoreWhite = countW;
}

int Reversi::getScoreBlack() const { return _scoreBlack; }

int Reversi::getScoreWhite() const { return _scoreWhite; }

std::vector<VectorPair> Reversi::getMoves() const { return _moves; }

bool Reversi::isGameOver()
{
	// We call getWinner in our isGameOver to update _isFinished.
	getWinner();
	return _isFinished;
}

bool Reversi::makeMove(int x, int y)
{
	// We need to verify that these coordinates are in our moves.
	VectorPair tempSquare;
	std::vector<VectorPair> moves = getMoves();

	// We could create two pointers to directly modifies each scores without the need of another function that will scan every squares of our board but it would be too tedious. The easiest is to scan the whole Board.

	for (auto e : moves)
	{
		if (e._availableSquare.first == x && e._availableSquare.second == y)
		{
			tempSquare._availableSquare = e._availableSquare;
			tempSquare._flippedSquares = e._flippedSquares;
			break;
		}
	}

	if (tempSquare._flippedSquares.size() != 0 && _board[x][y] == PlayerEmpty)
	{
		_board[x][y] = _current;

		// We then flip every of the stored flippedSqures
		for (auto e : tempSquare._flippedSquares)
		{
			_board[e.first][e.second] = _current;
		}
		
		// Use of ternary operator to switch between current player.
		_current = ((_current == PlayerBlack) ? PlayerWhite : PlayerBlack);

		// We could also update the score when we made a move.
		// updateScore();

		return true;
	}

	return false;
}

std::ostream & operator<<(std::ostream & os, const Reversi & game)
{
	for(auto & l : game._board)
	{
		for (auto & c : l)
		{
			switch(c)
			{
				case PlayerBlack:
					os << "B ";
					break;
				case PlayerWhite:
					os << "W ";
					break;
				case PlayerEmpty:
					os << ". ";
					break;
				// This case shouldn't exist
				default:
					os << "1 ";
			}
		}
		os << std::endl;
	}
	return os;
}

std::vector<std::pair<int, int>> Reversi::findPossibleSquares()
{
	std::vector<std::pair<int, int>> possibleSquares;
	Player opposite = ((_current == PlayerBlack) ? PlayerWhite : PlayerBlack);
	for (int l=0; l<8; l++)
	{
		for (int c=0; c<8; c++)
		{
			if (_board[l][c] == opposite)
			{
				/* We have to limit our verifications if we are verifying an out of board element.
				 * Because PlayerEmpty is 2, an out of range value could be considered as a valid one. */

				// 8 verifications for each directions.
				// We verify each time if the next square would still be in our board.
				if (_board[l-1][c] == PlayerEmpty && ((l-1 > -1 && l < 8) && (c > -1 && c < 8)))
				{
					possibleSquares.push_back(std::make_pair(l-1, c));
				}
				if (_board[l-1][c+1] == PlayerEmpty && ((l-1 > -1 && l < 8) && (c > -1 && c+1 < 8)))
				{
					possibleSquares.push_back(std::make_pair(l-1, c+1));
				}
				if (_board[l][c+1] == PlayerEmpty && ((l > -1 && l < 8) && (c > -1 && c+1 < 8)))
				{
					possibleSquares.push_back(std::make_pair(l, c+1));
				}
				if (_board[l+1][c+1] == PlayerEmpty && ((l > -1 && l+1 < 8) && (c > -1 && c+1 < 8)))
				{
					possibleSquares.push_back(std::make_pair(l+1, c+1));
				}
				if (_board[l+1][c] == PlayerEmpty && ((l > -1 && l+1 < 8) && (c > -1 && c < 8)))
				{
					possibleSquares.push_back(std::make_pair(l+1, c));
				}
				if (_board[l+1][c-1] == PlayerEmpty && ((l > -1 && l+1 < 8) && (c-1 > -1 && c < 8)))
				{
					possibleSquares.push_back(std::make_pair(l+1, c-1));
				}
				if (_board[l][c-1] == PlayerEmpty && ((l > -1 && l < 8) && (c-1 > -1 && c < 8)))
				{
					possibleSquares.push_back(std::make_pair(l, c-1));
				}
				if (_board[l-1][c-1] == PlayerEmpty && ((l-1 > -1 && l < 8) && (c-1 > -1 && c < 8)))
				{
					possibleSquares.push_back(std::make_pair(l-1, c-1));
				}
			}
		}
	}

	// We sort and remove any duplicates.
	std::sort(possibleSquares.begin(), possibleSquares.end());
	possibleSquares.erase(unique(possibleSquares.begin(), possibleSquares.end()), possibleSquares.end());
	
	return possibleSquares;
}

bool Reversi::findUsableSquares(std::vector<std::pair<int, int>> possibleSquares)
{
	Player opposite = ((_current == PlayerBlack) ? PlayerWhite : PlayerBlack);
	
	// We need to clear _moves at every turns
	_moves.clear();

	for(auto e : possibleSquares)
	{
		VectorPair usableSquare;
		usableSquare._availableSquare = e;

		int x = e.first, y = e.second;

		std::vector<std::pair<int, int>> tempFlipped;
		std::vector<std::pair<int, int>> tempDirectionFlipped;

		// After having the coordinates of our point, we go in 8 directions.

		// Up direction, we need to modify our position value to enter the loop and go in this direction while there is an opposite player in this direction.
		x--;
		
		// We verify each time if the current square is still in our board.
		while(_board[x][y] == opposite && ((x-1 > -1 && x < 8) && (y > -1 && y < 8)))
		{
			// We first add the current element in our temporary vector.
			tempDirectionFlipped.push_back(std::make_pair(x, y));

			// If the next element is ours, we can stop this direction and store every of these flipped squares into our general tempFlipped vector. We need to append the whole tempDirectionFlipped to tempFlipped (and not copy it because it would remove every of our previous squares).
			if (_board[x-1][y] == _current)
			{
				tempFlipped.insert(tempFlipped.end(), tempDirectionFlipped.begin(), tempDirectionFlipped.end());
				tempDirectionFlipped.clear();
				break;
			}

			// If the next element is empty, we can stop this direction and not store these element, this point in this direction is not valid.
			else if (_board[x-1][y] == PlayerEmpty)
			{
				tempDirectionFlipped.clear();
				break;
			}

			// We then continue to go forward on our direction.
			x--;
		}
	
		// We store into x and y the initial value of our point.
		x = e.first, y = e.second;

		// Upper Right direction
		x--; y++;
		while(_board[x][y] == opposite && ((x-1 > -1 && x < 8) && (y > -1 && y+1 < 8)))
		{
			tempDirectionFlipped.push_back(std::make_pair(x, y));
			if (_board[x-1][y+1] == _current)
			{
				tempFlipped.insert(tempFlipped.end(), tempDirectionFlipped.begin(), tempDirectionFlipped.end());
				tempDirectionFlipped.clear();
				break;
			}
			else if (_board[x-1][y+1] == PlayerEmpty)
			{
				tempDirectionFlipped.clear();
				break;
			}
			x--;
			y++;
		}

		x = e.first, y = e.second;

		// Right direction
		y++;
		while(_board[x][y] == opposite && ((x > -1 && x < 8) && (y > -1 && y+1 < 8)))
		{
			tempDirectionFlipped.push_back(std::make_pair(x, y));
			if (_board[x][y+1] == _current)
			{
				tempFlipped.insert(tempFlipped.end(), tempDirectionFlipped.begin(), tempDirectionFlipped.end());
				tempDirectionFlipped.clear();
				break;
			}
			
			else if (_board[x][y+1] == PlayerEmpty)
			{
				tempDirectionFlipped.clear();
				break;
			}
			y++;
		}

		x = e.first, y = e.second;

		// Bottom Right direction
		x++; y++;
		while(_board[x][y] == opposite && ((x > -1 && x+1 < 8) && (y > -1 && y+1 < 8)))
		{
			tempDirectionFlipped.push_back(std::make_pair(x, y));
			if (_board[x+1][y+1] == _current)
			{
				tempFlipped.insert(tempFlipped.end(), tempDirectionFlipped.begin(), tempDirectionFlipped.end());
				tempDirectionFlipped.clear();
				break;
			}
			
			else if (_board[x+1][y+1] == PlayerEmpty)
			{
				tempDirectionFlipped.clear();
				break;
			}
			x++;
			y++;
		}
		
		x = e.first, y = e.second;

		// Bottom direction
		x++;
		while(_board[x][y] == opposite && ((x > -1 && x+1 < 8) && (y > -1 && y < 8)))
		{
			tempDirectionFlipped.push_back(std::make_pair(x, y));
			if (_board[x+1][y] == _current)
			{
				tempFlipped.insert(tempFlipped.end(), tempDirectionFlipped.begin(), tempDirectionFlipped.end());
				tempDirectionFlipped.clear();
				break;
			}
			else if (_board[x+1][y] == PlayerEmpty)
			{
				tempDirectionFlipped.clear();
				break;
			}
			x++;
		}

		x = e.first, y = e.second;

		// Bottom Left direction
		x++; y--;
		while(_board[x][y] == opposite && ((x > -1 && x+1 < 8) && (y-1 > -1 && y < 8)))
		{
			tempDirectionFlipped.push_back(std::make_pair(x, y));
			if (_board[x+1][y-1] == _current)
			{
				tempFlipped.insert(tempFlipped.end(), tempDirectionFlipped.begin(), tempDirectionFlipped.end());
				tempDirectionFlipped.clear();
				break;
			}
			else if (_board[x+1][y-1] == PlayerEmpty)
			{
				tempDirectionFlipped.clear();
				break;
			}
			x++;
			y--;
		}

		x = e.first, y = e.second;

		// Left direction
		y--;
		while(_board[x][y] == opposite && ((x > -1 && x < 8) && (y-1 > -1 && y < 8)))
		{
			tempDirectionFlipped.push_back(std::make_pair(x, y));
			if (_board[x][y-1] == _current)
			{
				tempFlipped.insert(tempFlipped.end(), tempDirectionFlipped.begin(), tempDirectionFlipped.end());
				tempDirectionFlipped.clear();
				break;
			}
			else if (_board[x][y-1] == PlayerEmpty)
			{
				tempDirectionFlipped.clear();
				break;
			}
			y--;
		}

		x = e.first, y = e.second;

		// Upper Left direction
		x--; y--;
		while(_board[x][y] == opposite && ((x-1 > -1 && x < 8) && (y-1 > -1 && y < 8)))
		{
			tempDirectionFlipped.push_back(std::make_pair(x, y));
			if (_board[x-1][y-1] == _current)
			{
				tempFlipped.insert(tempFlipped.end(), tempDirectionFlipped.begin(), tempDirectionFlipped.end());
				tempDirectionFlipped.clear();
				break;
			}
			else if (_board[x-1][y-1] == PlayerEmpty)
			{
				tempDirectionFlipped.clear();
				break;
			}
			x--;
			y--;
		}

		if (tempFlipped.size() != 0)
		{
			// We sort the tempFlipped vector to have an easier printing.
			std::sort(tempFlipped.begin(), tempFlipped.end());
			usableSquare._flippedSquares = tempFlipped;
			_moves.push_back(usableSquare);
		}
	}

	// If there are no usableSquares we can change the current player, we will change the turn in our main.
	if (_moves.size() == 0)
	{
		/* If our current player can't play we add one to this value. Then, if the previous turn the player skipped his turn 
		* and that the other player has to skip the turn, then we end the party. _isImpossiblePlayer == 2 is our signal. */
		_isImpossiblePlay += 1;
		_current = ((_current == PlayerBlack) ? PlayerWhite : PlayerBlack);
	}

	else
	{
		// If the previous turn the player skipped his turn but we can play we give 0 to this value, everything is fine.
		if (_isImpossiblePlay == 1)
			_isImpossiblePlay = 0;
	}

	return (_moves.size() != 0);
}